package com.collect.droplets.game.entity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.graphics.Path
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import kotlin.math.cos
import kotlin.math.sin

data class FallingEntity(val view: View,
                         private val stepDuration: Long,
                         private val step: Int) {

    private var isFalling = false

    companion object{
        private const val FALL_DURATION = 300L
    }

    private fun getPath(newAngle: Float): Path = Path().apply {
        moveTo(view.x, view.y)
        val angleInRad = Math.toRadians(newAngle.toDouble()/10)
        val newX = ((view.x * cos(angleInRad)) - ((view.y+step) * sin(angleInRad))).toFloat()
//        val newY = (((view.y+step) * cos(angleInRad)) + (view.x * sin(angleInRad))).toFloat()
        lineTo(newX, view.y + step)
        moveTo(newX, view.y + step)
    }

    fun go(newAngle: Float, fullHeight: Int): Boolean {
        if (view.y > fullHeight)
            return false
        view.rotation = newAngle
        val path = getPath(newAngle)
        val animator = ObjectAnimator.ofFloat(view, "x", "y", path)
            .setDuration(stepDuration)
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.start()
        return true
    }

    fun fallDown(deleteAfter: () -> Unit) {
        if (isFalling)
            return
        isFalling = true
        val path = Path().apply {
            moveTo(view.x, view.y)
            lineTo(view.x, view.y + view.height)
            moveTo(view.x, view.y + view.height)
        }
        val animator = ObjectAnimator.ofFloat(view, "x", "y", path)
            .setDuration(FALL_DURATION)
        val animatorScaleX = ObjectAnimator.ofFloat(view, "scaleX", 0.0f)
            .setDuration(FALL_DURATION)
        val animatorScaleY = ObjectAnimator.ofFloat(view, "scaleY", 0.0f)
            .setDuration(FALL_DURATION)
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                deleteAfter()
            }
        })
        animatorScaleX.start()
        animatorScaleY.start()
        animator.start()
    }
}