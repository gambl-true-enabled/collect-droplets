package com.collect.droplets.game

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.util.Range
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.get
import com.collect.droplets.R
import com.collect.droplets.game.entity.FallingEntity
import kotlin.random.Random

class VolcanoAndDropletsView: RelativeLayout {

    companion object{
        private val GENERATE_TIME = 1000L to 4000L
        private const val DRAW_STEP_TIME = 30L
    }

    private val coinsList = mutableListOf<FallingEntity>()
    private val enemyList = mutableListOf<FallingEntity>()

    private lateinit var getRotationFunc: () -> Float
    private lateinit var finishCallback: (score: Int) -> Unit

    lateinit var scoreCallback: (score: Int) -> Unit

    private lateinit var generatorRecursiveCoin: Runnable
    private lateinit var generatorRecursiveEnemy: Runnable
    private lateinit var drawerRecursive: Runnable

//    private lateinit var textScoreLife: TextView

    private var score = 0
    private var life = 3

    private val volcanoY = Range(resources.displayMetrics.heightPixels - (170 * resources.displayMetrics.density).toInt(), resources.displayMetrics.heightPixels - (155 * resources.displayMetrics.density).toInt())
    private val volcanoX = Range(resources.displayMetrics.widthPixels/2 - (40 * resources.displayMetrics.density).toInt(), resources.displayMetrics.widthPixels/2 + (40 * resources.displayMetrics.density).toInt())

    init {
        generatorRecursiveCoin = Runnable {
            if (life == 0)
                return@Runnable
            nextGenerateCoin()
            val nextTime = Random.nextLong(GENERATE_TIME.first, GENERATE_TIME.second)
            postDelayed(generatorRecursiveCoin, nextTime)
        }
        generatorRecursiveEnemy = Runnable {
            if (life == 0)
                return@Runnable
            nextGenerateEnemy()
            val nextTime = Random.nextLong(GENERATE_TIME.first, GENERATE_TIME.second)
            postDelayed(generatorRecursiveEnemy, nextTime)
        }
        drawerRecursive = Runnable {
            if (life == 0)
                return@Runnable
            nextDraw()
            postDelayed(drawerRecursive, DRAW_STEP_TIME)
        }
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        prepareViews()
    }

    private fun prepareViews() {
//        textScoreLife = inflateTextView()
//        addView(textScoreLife)
        generatorRecursiveCoin.run()
        generatorRecursiveEnemy.run()
        drawerRecursive.run()
        inflateVolcanoView()
//        applyScoreLife()
    }

    fun setRotationGetter(getter: () -> Float) {
        getRotationFunc = getter
    }

    fun setFinishCallback(callback: (score: Int) -> Unit) {
        finishCallback = callback
    }

    private fun inflateCoinView(): View =
        LayoutInflater.from(context).inflate(R.layout.view_coin, this, false)

    private fun inflateEnemyView(): View =
        LayoutInflater.from(context).inflate(R.layout.view_enemy, this, false)

    private fun inflateVolcanoView() =
        LayoutInflater.from(context).inflate(R.layout.view_volcano, this, true)

//    private fun inflateTextView(): TextView =
//        LayoutInflater.from(context).inflate(R.layout.view_score, this, false) as TextView

    private fun nextGenerateCoin() {
        val coinView = inflateCoinView()
        addView(coinView)
        coinView.x = Random.nextInt(0, resources.displayMetrics.widthPixels - 50).toFloat()
        coinView.y = -50f
        coinsList.add(FallingEntity(
            coinView,
            DRAW_STEP_TIME,
            Random.nextInt(8, 30)
        ))
    }

    private fun nextGenerateEnemy() {
        val enemyView = inflateEnemyView()
        addView(enemyView)
        enemyView.x = Random.nextInt(0, resources.displayMetrics.widthPixels - 50).toFloat()
        enemyView.y = -50f
        enemyList.add(FallingEntity(
            enemyView,
            DRAW_STEP_TIME,
            Random.nextInt(15, 40)
        ))
    }

    private fun nextDraw() {
        if (this::getRotationFunc.isInitialized) {
            coinsList.removeAll(coinsList.doAll {
                coinsList.remove(it)
                removeView(it.view)
                riceScore()
            })
            enemyList.removeAll(enemyList.doAll {
                enemyList.remove(it)
                removeView(it.view)
                downLife()
            })
        }
    }

    private fun List<FallingEntity>.doAll(fallInVulcan: (item: FallingEntity) -> Unit): List<FallingEntity> {
        val deleteList = mutableListOf<FallingEntity>()
        forEach {
            if (volcanoY.contains(it.view.y.toInt()) && volcanoX.contains(it.view.x.toInt() + it.view.width/2)) {
                it.fallDown{ fallInVulcan(it) }
            } else {
                val alive = it.go(getRotationFunc(), if (measuredHeight > 0) measuredHeight else resources.displayMetrics.heightPixels)
                if (alive.not()) {
                    removeView(it.view)
                    deleteList.add(it)
                }
            }
        }
        return deleteList
    }

    private fun downLife() {
//        life--
//        applyScoreLife()
//        if (life == 0)
        finishCallback(score)
    }

    private fun riceScore() {
        score++
        scoreCallback(score)
//        applyScoreLife()
    }

//    private fun applyScoreLife() {
//        textScoreLife.text = String.format(
//            resources.getString(R.string.score_life),
//            score.toString(),
//            life.toString()
//        )
//    }
}