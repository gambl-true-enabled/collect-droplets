package com.collect.droplets

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.collect.droplets.di.AppComponent
import com.collect.droplets.di.DaggerAppComponent
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.onesignal.OneSignal
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig

class App: Application() {

    companion object{
        private lateinit var facebookLogger: AppEventsLogger

        fun facebookEvent(event: String) {
            if (this::facebookLogger.isInitialized) {
                facebookLogger.logEvent(event)
            }
        }
    }

    private val conversionListener: AppsFlyerConversionListener = object : AppsFlyerConversionListener {
        override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
            conversionData.forEach {
                Log.i("CrasherConversion1", "${it.key} ${it.value}")
            }
        }
        override fun onConversionDataFail(errorMessage: String) {
            Log.i("CrasherConversion2", "onConversionDataFail $errorMessage")
        }
        override fun onAppOpenAttribution(attributionData: Map<String, String>) {
            attributionData.forEach {
                Log.i("CrasherConversion3", "${it.key} ${it.value}")
            }
        }
        override fun onAttributionFailure(errorMessage: String) {
            Log.i("CrasherConversion4", "onAttributionFailure $errorMessage")
        }
    }


    override fun onCreate() {
        super.onCreate()
        AppComponent.init(DaggerAppComponent.builder().build())

        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        facebookLogger = AppEventsLogger.newLogger(this)

        AppsFlyerLib.getInstance().init("jzvY5PTmYwVkSNoTnySxYj", conversionListener, this)
        AppsFlyerLib.getInstance().startTracking(this)

        // Logging set to help debug issues, remove before releasing your app.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()

        val config = YandexMetricaConfig.newConfigBuilder("55bfa57a-f9c9-4cdb-8c8c-5809d58b057b").build()
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this)
    }
}