package com.collect.droplets

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.collect.droplets.di.AppComponent
import com.collect.droplets.ui.play.GamePlayFragment
import com.collect.util_sensor.SensorControllerApi
import javax.inject.Inject

class GameActivity : AppCompatActivity(), GamePlayFragment.Callback {

    @Inject
    lateinit var sensorControllerApi: SensorControllerApi

    override fun onCreate(savedInstanceState: Bundle?) {
        AppComponent.get().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        sensorControllerApi.initialize(this)
    }

    override fun getRotationGame(): Float = sensorControllerApi.getRotation()
}