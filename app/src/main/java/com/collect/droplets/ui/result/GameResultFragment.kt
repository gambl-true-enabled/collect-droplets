package com.collect.droplets.ui.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.collect.droplets.R
import kotlinx.android.synthetic.main.fragment_game_result.*
import kotlinx.android.synthetic.main.fragment_game_result.view.*

class GameResultFragment: Fragment() {

    private val args: GameResultFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_game_result, container, false)
//        view.button_main_menu.setOnClickListener {
//            view.findNavController().navigate(GameResultFragmentDirections.showMain())
//        }
        view.button_restart.setOnClickListener {
            view.findNavController().navigate(GameResultFragmentDirections.playAgain())
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        text_score_total?.text = String.format(
            resources.getString(R.string.score_total),
            args.collectScore
        )
    }
}