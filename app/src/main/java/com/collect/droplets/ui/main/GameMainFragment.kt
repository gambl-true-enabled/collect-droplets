package com.collect.droplets.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.collect.droplets.R
import kotlinx.android.synthetic.main.fragment_game_main.view.*

class GameMainFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_game_main, container, false)
        view.button_play.setOnClickListener {
            view.findNavController().navigate(GameMainFragmentDirections.playGame())
        }
        view.button_exit.setOnClickListener {
            activity?.finish()
        }
        return view
    }
}