package com.collect.droplets.ui.play

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.collect.droplets.R
import com.collect.droplets.ui.result.GameResultFragmentDirections
import kotlinx.android.synthetic.main.fragment_game_play.*
import kotlinx.android.synthetic.main.fragment_game_play.view.*

class GamePlayFragment: Fragment() {

    private var callback: Callback? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as Callback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_game_play, container, false)
        view.game_view.scoreCallback = {
            game_score?.text = String.format(
                resources.getString(R.string.score),
                it.toString()
            )
        }
        view.game_view.setRotationGetter {
            callback?.getRotationGame() ?: 0f
        }
        view.game_view.setFinishCallback { score ->
            view?.findNavController()?.navigate(GamePlayFragmentDirections.showResult(score))
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.game_view.scoreCallback.invoke(0)
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    interface Callback {
        fun getRotationGame(): Float
    }
}