package com.collect.droplets.ui.auth

import android.webkit.JavascriptInterface

class AndroidJavaScript(private val callback: Callback) {

    @JavascriptInterface
    fun onNeedAuth() = callback.onAuth()

    @JavascriptInterface
    fun onAuthorized() = callback.onAuthorized()

    interface Callback {

        fun onAuth()

        /**     Let user visit game
         * */
        fun onAuthorized()
    }
}