package com.collect.droplets.di

import com.collect.droplets.GameActivity
import com.collect.droplets.MainActivity
import com.collect.util_sensor.SensorModule
import dagger.Component
import java.lang.IllegalArgumentException
import java.lang.RuntimeException
import javax.inject.Singleton

@Component(
    modules = [SensorModule::class]
)
@Singleton
abstract class AppComponent {

    companion object{

        private var sAppComponent: AppComponent? = null

        fun init(component: AppComponent) {
            if (sAppComponent != null) {
                throw IllegalArgumentException("AppComponent is already initialized.")
            }
            sAppComponent = component
        }

        fun get(): AppComponent {
            sAppComponent ?: throw RuntimeException("You must call init before")
            return sAppComponent!!
        }
    }

    abstract fun inject(activity: GameActivity)
}