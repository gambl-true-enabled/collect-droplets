package com.collect.droplets.utils

import android.content.Context

private const val DEEP_TABLE = "com.deep.table.home"
private const val DEEP_ARGS = "com.deep.value.home"

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(DEEP_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(DEEP_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(DEEP_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(DEEP_ARGS, null)
}
