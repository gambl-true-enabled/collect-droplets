package com.collect.util_sensor

import android.app.Activity
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import javax.inject.Inject

internal class SensorControllerImpl @Inject constructor(): SensorControllerApi {

    private var lastRotation = 0f

    private val sensorListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

        }

        override fun onSensorChanged(event: SensorEvent?) {
            if (event == null) return
            if (event.values.size < 3) return

            val angle = event.values[1]*2
            lastRotation = angle*-100
        }
    }

    override fun initialize(activity: Activity) {
        activity.apply {
            val sensorManager = getSystemService(Activity.SENSOR_SERVICE)
            if (sensorManager is SensorManager) {
                sensorManager.registerListener(
                    sensorListener,
                    sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                    SensorManager.SENSOR_DELAY_GAME
                )
            }
        }
    }

    override fun getRotation(): Float = lastRotation
}