package com.collect.util_sensor

import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class SensorModule {

    @Binds
    @Singleton
    internal abstract fun provideController(sensor: SensorControllerImpl): SensorControllerApi
}