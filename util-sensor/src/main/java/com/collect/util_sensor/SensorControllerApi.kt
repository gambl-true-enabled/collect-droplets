package com.collect.util_sensor

import android.app.Activity

interface SensorControllerApi {

    fun initialize(activity: Activity)

    fun getRotation(): Float
}